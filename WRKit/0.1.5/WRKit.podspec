#
# Be sure to run `pod lib lint WRKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'WRKit'
  s.version          = '0.1.5'
  s.summary          = 'WebRTC SDK for iOS'
  s.swift_version    = '4'
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
WebRTC SDK for iOS
                       DESC

  s.homepage         = 'https://bitbucket.org/wildmud/webrtc-sdk-swift-specs'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'proprietary', :file => 'LICENSE' }
  s.author           = { 'wildmud' => 'aleiku@gmail.com' }
  s.source           = { :git => 'https://aleiku@bitbucket.org/wildmud/webrtc-sdk-swift.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'WRKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'WRKit' => ['WRKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'Socket.IO-Client-Swift', '~> 12.1.2'
   s.dependency 'GoogleWebRTC', '1.1.25461'
   s.pod_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' }
end

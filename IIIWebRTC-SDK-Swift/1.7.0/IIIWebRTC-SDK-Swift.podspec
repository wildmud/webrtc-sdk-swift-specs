#
# Be sure to run `pod lib lint WRKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IIIWebRTC-SDK-Swift'
  s.module_name      = 'WRKit'
  s.version          = '1.7.0'
  s.summary          = 'WebRTC SDK for iOS'
  s.swift_version    = '4'
  s.description      = <<-DESC
WebRTC Swift SDK for iOS
New remove device token  function
openCamera / closeCamera
onMessage變更為不過濾type="msg"
新增 localVideoSize
新增 Event
    case localVideoSizeChange
    case streamingSizeChange
                       DESC

  s.homepage         = 'https://bitbucket.org/wildmud/webrtc-sdk-swift-specs'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'proprietary', :file => 'LICENSE' }
  s.author           = { 'wildmud' => 'aleiku@gmail.com' }
  s.source           = { :git => 'https://aleiku@bitbucket.org/wildmud/webrtc-sdk-swift.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'WRKit/Classes/**/*'
  
  # s.resource_bundles = {
  #   'WRKit' => ['WRKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'Socket.IO-Client-Swift', '~> 12.1.2'
   s.dependency 'GoogleWebRTC', '1.1.25461'
   s.pod_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' }
end
